#!/usr/bin/env python
# -*- encoding: utf8 -*-
import subprocess as sup
import os


def get_metadata(dataset):
    """ Simple metadata

    """

    geot = dataset.GetGeoTransform()
    lat0, lon0 = geot[3], geot[0]
    dlat, dlon = geot[5], geot[1]
    nrow = dataset.RasterYSize
    ncol = dataset.RasterXSize

    # Corner Coordinates
    # ==================
    # upper left
    lat_upp_left = lat0
    lon_upp_left = lon0
    # lower left
    lat_low_left = lat0 + dlat * nrow
    lon_low_left = lon0
    # upper right
    lat_upp_right = lat0
    lon_upp_right = lon0 + dlon * ncol
    # lower right
    lat_low_right = lat_low_left
    lon_low_right = lon_upp_right
    # center
    lat_center = (lat_upp_left + lat_low_left) / 2.0
    lon_center = (lon_upp_left + lon_upp_right) / 2.0

    metadata = {}
    metadata["driver"] = dataset.GetDriver().LongName
    metadata["ncol"] = dataset.RasterXSize
    metadata["nrow"] = dataset.RasterYSize
    metadata["geoproj"] = dataset.GetProjection()
    metadata["upp_left"] = [lat_upp_left, lon_upp_left]
    metadata["low_left"] = [lat_low_left, lon_low_left]
    metadata["upp_right"] = [lat_upp_right, lon_upp_right]
    metadata["low_right"] = [lat_low_right, lon_low_right]
    metadata["center"] = [lat_center, lon_center]
    metadata["pixel"] = [dlat, dlon]

    return metadata


def get_aspect(iDEM, trigonometric=None, zero_for_flat=None):
    """ Average aspect

        gdaldem aspect input_dem output_aspect_map
                       [-trigonometric] [-zero_for_flat]

        This command outputs a 32-bit float raster with values between 0° and 360°
        representing the azimuth that slopes are facing. The definition of the azimuth
        is such that : 0° means that the slope is facing the North, 90° it's facing the
        East, 180° it's facing the South and 270° it's facing the West (provided that
        the top of your input raster is north oriented). The aspect value -9999 is
        used as the nodata value to indicate undefined aspect in flat areas with slope=0.

        The following specifics options are available :

        -trigonometric:
            return trigonometric angle instead of azimuth.
            Thus 0° means East, 90° North, 180° West, 270° South

        -zero_for_flat:
           return 0 for flat areas with slope=0, instead of -9999

        By using those 2 options, the aspect returned by gdaldem aspect should be
        identical to the one of GRASS r.slope.aspect. Otherwise,
        it's identical to the one of Matthew Perry's aspect.cpp utility.
    """
    path, fname = os.path.split(iDEM)
    name, ext = fname.split('.')
    oDEM = os.path.join(path, name + "_aspect_gdaldem.tif")
    default_cmd = ["gdaldem", "aspect", iDEM, oDEM]
    extra = []
    if trigonometric:
        extra.append("-trigonometric")
    if zero_for_flat:
        extra.append("-zero_for_flat")

    sms = sup.check_output(default_cmd + extra)
    return oDEM, sms


def get_hillshading(iDEM, azimuth="315", altitude="45", zfactor="1", scale="1",
    combined=False):
    """ Generate a shaded relief map

        gdaldem hillshade input_dem output_hillshade
           [-z ZFactor (default=1)] [-s scale* (default=1)]
           [-az Azimuth (default=315)] [-alt Altitude (default=45)]
           [-alg ZevenbergenThorne] [-combined]
           [-compute_edges] [-b Band (default=1)] [-of format] [-co "NAME=VALUE"]* [-q]
    """
    path, fname = os.path.split(iDEM)
    name, ext = fname.split('.')
    oDEM = os.path.join(path, name + "_hillshade_gdaldem.tif")
    default_cmd = ["gdaldem", "hillshade", iDEM, oDEM]
    extra = []
    if azimuth != "315":
        extra.append("-az")
        extra.append(azimuth)
    if altitude != "45":
        extra.append("-alt")
        extra.append(altitude)
    if zfactor != "1":
        extra.append("-z")
        extra.append(zfactor)
    if scale != "1":
        extra.append("-s")
        extra.append(scale)
    if combined:
        extra.append("-combined")

    sms = sup.check_output(default_cmd + extra)
    return oDEM, sms


def get_slope(iDEM, slope_in="degrees", scale="1"):
    """ to generate a slope map

       This command will take a DEM raster and output a 32-bit float raster with
       slope values. You have the option of specifying the type of slope value you
       want: degrees or percent slope. In cases where the horizontal units differ
       from the vertical units, you can also supply a scaling factor.

       The value -9999 is used as the output nodata value.

       The following specific options are available :

       -p :
           if specified, the slope will be expressed as percent slope.
           Otherwise, it is expressed as degrees

       -s scale:
           ratio of vertical units to horizontal. If the horizontal unit of the
           source DEM is degrees (e.g Lat/Long WGS84 projection), you can use
           scale=111120 if the vertical units are meters (or scale=370400
           if they are in feet)
    """
    path, fname = os.path.split(iDEM)
    name, ext = fname.split('.')
    oDEM = os.path.join(path, name + "_slope_gdaldem.tif")
    default_cmd = ["gdaldem", "slope", iDEM, oDEM]
    extra = []
    if slope_in != "degrees":
        extra.append("-p")
    if scale != "1":
        extra.append("-s")
        extra.append(scale)
    sms = sup.check_output(default_cmd + extra)
    return oDEM, sms


def get_tri(iDEM):
    """
        TRI
           This command outputs a single-band raster with values computed from the
           elevation. TRI stands for Terrain Ruggedness Index, which is defined as the
           mean difference between a central pixel and its surrounding cells (see Wilson
           et al 2007, Marine Geodesy 30:3-35).

           The value -9999 is used as the output nodata value.

           There are no specific options.

           - To generate a Terrain Ruggedness Index (TRI) map from any GDAL-supported
           elevation raster:
           gdaldem TRI input_dem output_TRI_map
                       [-compute_edges] [-b Band (default=1)] [-of format] [-q]
    """
    path, fname = os.path.split(iDEM)
    name, ext = fname.split('.')
    oTRI = os.path.join(path, name + "_TRI_gdaldem.tif")
    cmd_tri = ["gdaldem", "TRI", iDEM, oTRI]
    sms_tri = sup.check_output(cmd_tri)
    return oTRI, sms_tri


def get_tpi(iDEM):
    """
       TPI
           This command outputs a single-band raster with values computed from the
           elevation. TPI stands for Topographic Position Index, which is defined as the
           difference between a central pixel and the mean of its surrounding cells
           (see Wilson et al 2007, Marine Geodesy 30:3-35).

           The value -9999 is used as the output nodata value.

           There are no specific options.

           - To generate a Topographic Position Index (TPI) map from any GDAL-supported
           elevation raster:
               gdaldem TPI input_dem output_TPI_map
                           [-compute_edges] [-b Band (default=1)] [-of format] [-q]

    """
    path, fname = os.path.split(iDEM)
    name, ext = fname.split('.')
    oTPI = os.path.join(path, name + "_TPI_gdaldem.tif")
    cmd_tpi = ["gdaldem", "TPI", iDEM, oTPI]
    sms_tpi = sup.check_output(cmd_tpi)
    return oTPI, sms_tpi


def get_ruggedness(iDEM):
    """ roughness
           This command outputs a single-band raster with values computed from the
           elevation. Roughness is the largest inter-cell difference of a central pixel
            and its surrounding cell, as defined in Wilson et al (2007, Marine Geodesy
            30:3-35).

           The value -9999 is used as the output nodata value.

           There are no specific options.

            - To generate a roughness map from any GDAL-supported elevation raster:
               gdaldem roughness input_dem output_roughness_map
                           [-compute_edges] [-b Band (default=1)] [-of format] [-q]

    """
    path, fname = os.path.split(iDEM)
    name, ext = fname.split('.')
    oROU = os.path.join(path, name + "_ruggedness_gdaldem.tif")
    cmd_rou = ["gdaldem", "roughness", iDEM, oROU]
    sms_rou = sup.check_output(cmd_rou)

    return oROU, sms_rou