#!/usr/bin/env python
# -*- encoding: utf8 -*-

#===============================================================================
# META
#===============================================================================

__version__ = "0.0.1"
__license__ = "GPL v3"
__author__ = "Emiliano López"
__email__ = "emiliano dot lopez at gmail dot com"
__url__ = "https://gitlab.com/emilopez/DEMystiPy"
__date__ = "2015-07-04"

from PyQt4 import QtCore, QtGui
from ui import main_ui
import sys
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.cm as cm
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavBar

import os
import gdal
import numpy as np

import demprocessor as dp


class MainWin (QtGui.QMainWindow, main_ui.Ui_MainWindow):

    def __init__(self, parent=None):

        QtGui.QMainWindow.__init__(self, parent)
        # ventana vincula gui
        self.win = main_ui.Ui_MainWindow()
        self.win.setupUi(self)
        # imagen dem
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.main_frame = self.win.verticalLayout.addWidget(self.canvas)
        # navbar matplotlib
        self.mpl_toolbar = NavBar(self.canvas, self.main_frame)
        self.win.verticalLayout.addWidget(self.mpl_toolbar)
        # Global variables
        self.computed = {}
        self.working = None

        # eventos
        # =======
        # Figure mousemove
        self.figure.canvas.mpl_connect("motion_notify_event", self.mouse_move)
        # Menu
        self.win.actionOpen_DEM.triggered.connect(self.open_dem)
        self.win.actionAbout.triggered.connect(self.about)
        # Compute buttons
        self.win.compute_aspect.clicked.connect(self.compute_aspect)
        self.win.compute_hillshading.clicked.connect(self.compute_hillshading)
        self.win.compute_slope.clicked.connect(self.compute_slope)
        self.win.compute_ruggedness.clicked.connect(self.compute_ruggedness)
        # Display obj and colormap lists
        #self.win.display_object_lw.currentItemChanged.connect(self.display_object_changed)
        self.win.display_object_lw.itemDoubleClicked.connect(self.display_object_changed)
        self.win.colormap_lw.itemClicked.connect(self.set_colormap)

    def open_dem(self):
        """ Open and display GeoTiff DEM

            Show a file dialog, clean the object display list and invoke
            a function to display a raster

        """
        self.dem_fn = str(QtGui.QFileDialog.getOpenFileName(self, 'Open DEM File'))
        if self.dem_fn:
            self.win.display_object_lw.clear()
            self.display_raster(self.dem_fn)

    def display_raster(self, iFile, colormap=None):
        """ Display a raster filename

        """
        self.figure.clear()
        dataset = gdal.Open(iFile, gdal.GA_ReadOnly)
        band = dataset.GetRasterBand(1)
        self.show_metadata(dataset)
        self.dem = band.ReadAsArray().astype(np.float)
        self.ax = self.figure.add_subplot(111)
        if colormap:
            colormap = eval("cm." + colormap)
        image = self.ax.imshow(self.dem, cmap=colormap)
        self.figure.colorbar(image, pad=0.01)
        # márgenes pequeños
        self.figure.tight_layout()
        self.canvas.draw()
        self.working = iFile
        self.add_display_object(iFile)

    def mouse_move(self, event):
        """ Captura posición del mouse sobre imagen (menú imágenes)
        """
    def add_display_object(self, iFile):
        """ Manage a global dictionary

            Global dictionary self.computed to store the filename of each raster file
            and its path, and add the item to a list widgest
        """

        path, fname = os.path.split(iFile)
        # path of fname file

        if not fname in self.computed:
            self.win.display_object_lw.addItem(fname)
        self.computed[fname] = path

    def show_metadata(self, dataset):
        """Show metadata from gdalinfo
        """
        meta = dp.get_metadata(dataset)
        # set row and col
        self.win.metadata_tw.setRowCount(len(meta))
        self.win.metadata_tw.setColumnCount(2)
        # insert keys of metadata in fisrt column
        self.win.metadata_tw.setItem(0, 0, QtGui.QTableWidgetItem('Driver'))
        self.win.metadata_tw.setItem(1, 0, QtGui.QTableWidgetItem('Geo proj'))
        self.win.metadata_tw.setItem(2, 0, QtGui.QTableWidgetItem('Pixel size'))
        self.win.metadata_tw.setItem(3, 0, QtGui.QTableWidgetItem('XSize'))
        self.win.metadata_tw.setItem(4, 0, QtGui.QTableWidgetItem('YSize'))
        self.win.metadata_tw.setItem(5, 0, QtGui.QTableWidgetItem('Upper left'))
        self.win.metadata_tw.setItem(6, 0, QtGui.QTableWidgetItem('Lower left'))
        self.win.metadata_tw.setItem(7, 0, QtGui.QTableWidgetItem('Upper right'))
        self.win.metadata_tw.setItem(8, 0, QtGui.QTableWidgetItem('Lower right'))
        self.win.metadata_tw.setItem(9, 0, QtGui.QTableWidgetItem('Center'))
        # insert metada values on second column
        self.win.metadata_tw.setItem(0, 1, QtGui.QTableWidgetItem(str(meta['driver'])))
        self.win.metadata_tw.setItem(1, 1, QtGui.QTableWidgetItem(str(meta['geoproj'])))
        self.win.metadata_tw.setItem(2, 1, QtGui.QTableWidgetItem(str(meta['pixel'])))
        self.win.metadata_tw.setItem(3, 1, QtGui.QTableWidgetItem(str(meta['ncol'])))
        self.win.metadata_tw.setItem(4, 1, QtGui.QTableWidgetItem(str(meta['nrow'])))
        self.win.metadata_tw.setItem(5, 1, QtGui.QTableWidgetItem(str(meta['upp_left'])))
        self.win.metadata_tw.setItem(6, 1, QtGui.QTableWidgetItem(str(meta['low_left'])))
        self.win.metadata_tw.setItem(7, 1, QtGui.QTableWidgetItem(str(meta['upp_right'])))
        self.win.metadata_tw.setItem(8, 1, QtGui.QTableWidgetItem(str(meta['low_right'])))
        self.win.metadata_tw.setItem(9, 1, QtGui.QTableWidgetItem(str(meta['center'])))

    def compute_aspect(self):
        """ Compute average aspect

            Get GUI options and call get_aspect(), add item to display list
        """
        system = str(self.win.aspect_system_cb.currentText())
        zero_for_flat = str(self.win.aspect_no_slope_cb.currentText())

        if system != "trigonometric":
            system = None
        if zero_for_flat != "0":
            zero_for_flat = None
        oASP, sms = dp.get_aspect(self.dem_fn, system, zero_for_flat)
        self.add_display_object(oASP)

    def compute_hillshading(self):
        """ Compute hillshading

            Get GUI options and call get_hillshading(), add item to display list
        """
        #iDEM, azimuth=None, altitude=None, zfactor=None, scale=None, combined=None

        azi = str(self.win.hill_azimuth_sb.value())
        alt = str(self.win.hill_altitude_sb.value())
        zfac = str(self.win.hill_zfactor_sb.value())
        scale = str(self.win.hill_scale_sb.value())
        combined = self.win.hill_combined_chb.isChecked()

        oHILL, sms = dp.get_hillshading(self.dem_fn, azi, alt, zfac, scale, combined)
        self.add_display_object(oHILL)

    def compute_slope(self):
        """ Compute slope

            Get GUI options, call get_slope(), add item to display list
        """
        slope_in = str(self.win.slope_system_cb.currentText())
        scale = str(self.win.slope_scale_sb.value())
        oSLOPE, sms = dp.get_slope(self.dem_fn, slope_in, scale)
        self.add_display_object(oSLOPE)

    def compute_ruggedness(self):
        """ Compute tri, tpi and ruggedness

            Get GUI options, call get_tri(), get_tpi(), get_rou() add item to display
        """
        tri = self.win.roughness_tri_chb.isChecked()
        tpi = self.win.roughness_tpi_chb.isChecked()
        rou = self.win.roughness_roug_chb.isChecked()
        if tri:
            oTRI, sms_tri = dp.get_tri(self.dem_fn)
            self.add_display_object(oTRI)
        if tpi:
            oTPI, sms_tpi = dp.get_tpi(self.dem_fn)
            self.add_display_object(oTPI)
        if rou:
            oROU, sms_rou = dp.get_ruggedness(self.dem_fn)
            self.add_display_object(oROU)

    def about(self):
        """ About Dialog with project info

        """
        QtGui.QMessageBox.about(self, self.tr("Acerca de..."),
        self.tr("DEMystiPY\n\n"
                "https://gitlab.com/emilopez/DEMystiPy\n"
                "Version: 0.2 \n\n"
                "Autor: Emiliano Lopez \n"
                "elopez [at] fich.unl.edu.ar\n"
                "twitter: @yosobreip\n"
                "Fecha: Ago 2015"))

    def display_object_changed(self, curr):
        """ Update raster

            Get current item text, and display the raster file
        """
        rasterFile = os.path.join(self.computed[str(curr.text())], str(curr.text()))
        self.working = rasterFile
        self.display_raster(rasterFile)

    def set_colormap(self, curr):
        """ Update colormap
        """
        iFile = self.working
        self.display_raster(iFile, str(curr.text()))


def main():

    app = QtGui.QApplication(sys.argv)
    win = MainWin()
    win.show()
    sys.exit(app.exec_())

if __name__ == '__main__':

    main()
