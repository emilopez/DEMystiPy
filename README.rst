Proyecto DEMystiPy
==================

DEMystiPy es un proyecto abierto, colaborativo que consiste en:

- Un **tutorial** explicativo sobre el desarrollo de software de procesamiento de DEMs
- Un **Software** para el procesamiento y visualización de DEMs

El software en si ha sido el resultado del tutorial mencionado, por lo que se complementan y retroalimentan entre sí. 

Tutorial
--------

El tutorial ha sido escrito en texto plano utilizando el formato ReStructuredText. Para generar el pdf correspondiente es necesario instalar rst2pdf y ejecutar:

.. parsed-literal::

    rst2pdf Tutorial.rst 

Puede navegar la versión en rst haciendo clic en `Tutorial.rst <https://gitlab.com/emilopez/DEMystiPy/blob/master/Tutorial.rst>`__

También puede descargar la versión del `tutorial en pdf <https://gitlab.com/emilopez/DEMystiPy/blob/master/Tutorial.pdf>`__ haciendo click derecho y seleccionando "Guardar enlace como..."


- El tutorial está bajo Licencia Creative Commons: `Atribución-CompartirIgual 4.0 Internacional <http://creativecommons.org/licenses/by-sa/4.0/>`__ 


Software DEMystiPy
------------------

El software ha sido ejectutado sin inconvenientes en distintas plataformas GNU/Linux, aún no ha sido probado en sistemas Windows, por lo que son bienvenidas las contribuciones de los usuarios.

DEMystiPy aún se encuentra en fase de desarrollo por lo que puede contener errores y comportamientos indeseados.

- Versión: v0.0.1

Requerimientos
~~~~~~~~~~~~~~

Para ejecutar DEMystiPy debe instalar las dependencias que se encuentran detalladas en requirements.txt


Características
~~~~~~~~~~~~~~~

-  Licencia: GPL v3
-  Interfaz de usuario gráfica (GUI)
-  Visualización de DEMs y mapas raster calculados

   -  zoom in/out
   -  exportar como imagen png, jpg, etc.

-  Elección del mapa de colores para visualización
-  Referencia del valor numérico del color
-  Muestra información global del DEM:

   -  Driver
   -  Proyección
   -  Tamaño de píxel
   -  XSize, YSize
   -  Coordenadas de la imagen (superior izquierda, inferior izquierda,
      superior derecha, inferior derecha, centro)

-  Cálculos realizados utilizando la herramienta gdaldem:

   -  Average Aspect
   -  Hillshading
   -  Slope
   -  Roughness
   -  Terrain Ruggedness Index (TRI)
   -  Topographic Position Index (TPI)

Instalación
~~~~~~~~~~~

- Descargar el proyecto completo
- Descomprimir
- Ejecutar el programa demystipy.py ubicado en bajo el directorio DEMystiPy/DEMystiPy

TODO List
~~~~~~~~~

- [ ] Soporte para visualización 3D
- [ ] Incorporar cálculos provistos por PyDEM
- [ ] Incorporar cálculos provistos por PyGeoprocessing



