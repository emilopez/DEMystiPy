
Desarrollo de software aplicado al procesamiento de DEMs
========================================================

:Autor: Emiliano López - emiliano.lopez@gmail.com

:Fecha: |date|


Introducción
------------

En la actualidad existe una gran cantidad de software para el análisis y
procesamiento de DEMs. Algunos de ellos son software propietario, es
decir, el usuario no es más que meramente un operador de la aplicación,
por otro lado existen aquellos que son distribuidos bajo el concepto de
`software libre <http://www.gnu.org/philosophy/free-sw.html>`__, esto
significa que el usuario tiene la posibilidad de copiarlo,
redistribuirlo y modificarlo sin infringir ninguna licencia. Para esto
último es indispensable contar con el acceso al código fuente con el que
se desarrolló el programa, generalmente distribuido con el mismo
software o bien disponibles en repositorios web de software libre, por
ejemplo, github.com, gitlab.com, sourceforge.com, bitbucket.com, etc.

Además del software libre previamente mencionado, generalmente
auto-contenido, también existe una gran cantidad de bibliotecas
disponibles para el procesamiento de DEMs. Las bibliotecas de software
(erróneamente denominadas librerías por su traducción del inglés
library) son subprogramas que realizan tareas específicas y pueden ser
incluidas en el desarrollo de un paquete software.

El presente trabajo es una guía introductoria para desarrollar software
utilizando y aprovechando la vasta cantidad de programas y bibliotecas
de libre acceso y disponibles en internet que facilitan enormemente las
tareas de procesamiento y visualización de DEMs.

Objetivos
---------

Objetivo general
~~~~~~~~~~~~~~~~

Desarrollar una tutorial introductorio sobre el desarrollo de software
para el procesamiento de DEMs haciendo uso de herramientas y software
libre disponible en la web.

Objetivos específicos
~~~~~~~~~~~~~~~~~~~~~

-  Describir someramente los módulos/bibliotecas de software libre
   necesario
-  Detallar el modo de uso de estas herramientas
-  Describir el escenario necesario para realizar desarrollos propios
-  Utilizar dentro del software propio las herramientas de software
   libre disponibles

Materiales y métodos
--------------------

Lenguaje de programación
~~~~~~~~~~~~~~~~~~~~~~~~

El lenguaje de programación utilizado es Python, dentro de las
características mas salientes que fundamentan su elección se mencionan
las siguientes:

-  Lenguaje en constante crecimiento
-  Comunidad de desarrolladores inmensa
-  Excelente ecosistema para el desarrollo de software científico
-  Bibliotecas de software y comunidad específicamente orientada al
   ámbito científico

Los DEMs se distribuyen bajo diferentes formatos de archivos binarios
que cuentan con estructuras similares, sin embargo, deben ser abstraídas
para poder acceder a la información contenida de una manera uniforme,
independientemente del formato. Aquí es donde entran en juego las
bibliotecas (subprogramas) específicas que facilitan enormemente la
tarea de lectura e interpretación de los datos.

Bibliotecas
~~~~~~~~~~~

GDAL/OGR y PROJ.4
^^^^^^^^^^^^^^^^^

La biblioteca por excelencia para el procesamiento de datos raster es
**GDAL** (del inglés `Geospatial Data Abstraction
Library <www.gdal.org>`__). La gran mayoría del software de
procesamiento de imágenes satelitales, ya sea libre o propietario (entre
ellos DEMs), la utiliza como insumo básico.

GDAL funciona como un traductor para formatos de datos geoespaciales
raster o vectorial, y el distribuía bajo licencia libre X/MIT de la
Fundación Geoespacial de Código Abierto. Presenta un único modelo de
abstracción de datos raster para la aplicación/programa que la utilice
(invoque) para todos los `formatos de archivos
soportados <http://www.gdal.org/formats_list.html>`__ y cuenta con una
serie de programas utilitarios para conversión de formatos y
procesamiento.

GDAL también incluye la biblioteca **OGR**, por lo que provee soporte de
lectura/escritura para una variedad de datos vectoriales incluyendo ESRI
Shapefiles, S-57, SDTS, PostGIS, Oracle Spatial, Mapinfo mid/mif y
formato TAB.

Inicialmente GDAL y OGR se distribuían en forma separada, sin embargo, a
partir de las últimas versiones se distribuyen en forma conjunta, por lo
que en la instalación de GDAL se incluye OGR y se las suele denominar
como un único conjunto de bibliotecas **GDAL/OGR**.

Otra biblioteca de gran utilidad es
`PROJ.4 <https://github.com/OSGeo/proj.4>`__, cuya función consiste en
realizar transformaciones entre proyecciones geográficas (lat/lon) y
cartográficas (x/y) y viceversa, así como también entre diferentes
proyecciones cartográficas en forma directa.

Tanto GDAL/OGR como PROJ.4 son consideradas bibliotecas raíz, lo que
significa que son utilizadas por muchos paquetes de software como
también por otras bibliotecas y, ambas cuentan con su versión para ser
invocadas en forma transparente desde Python. A continuación se observa
un diagrama con el vínculo entre estas bibliotecas y las diferentes
aplicaciones.

.. figure:: img/common_libs.png
    :width: 1400px

    Bibliotecas comúnmente utilizadas

PyDEM
^^^^^

`PyDEM <https://github.com/creare-com/pydem>`__, recientemente
`presentado <https://www.youtube.com/watch?v=bGulPZh_-Mo>`__ en la
`Conferencia Anual de Python
Científico <http://www.scipy2015.scipy.org>`__, es un paquete de
software para análisis topográfico escrito en Python. A partir de un
modelo de elevación raster permite calcular los siguientes mapas:

-  Pendientes (slope)
-  Aspecto (aspect)
-  Áreas de aporte (upstream area)
-  Índice topográfico de humedad (topographic wetness)

PyDEM puede ser usado como un programa en si mismo o bien como un módulo
(en la jerga de Python suele denominarse módulos a las bibliotecas) e
invocar sus funciones. Permite operar en rasters individuales o por
lotes de archivos incluidos en un directorio. Utiliza la biblioteca GDAL
y para ciertos cálculos
`TauDEM <hydrology.usu.edu/taudem/taudem5/index.html>`__

PyGeoprocessing
^^^^^^^^^^^^^^^

`PyGeoprocessing <https://bitbucket.org/richpsharp/pygeoprocessing>`__
es una biblioteca Python que provee un conjunto de operaciones raster,
vectoriales e hidrológicas comúnmente utilizadas en procesamiento GIS.
Sus funcionalidades son equivalentes a las raster encontradas en
ArcGIS/QGIS, estadísticas en ArcGIS y, routinas hidrológicas en
ArcGIS/GRASS/TauDEM.

Según se informa en el sitio web del proyecto, PyGeoprocessing fue
desarrollado como una biblioteca de procesamiento GIS programable, de
código abierto y libre cuyo diseño prioriza la eficiencia de cálculos y
consumo de memoria, fácil instalación y compatibilidad con otros
software de licencias abiertas y propietarias. Específicamente, las
funcionalidades incluidas son (textual, extraído del sitio web):

-  programmable raster algebra routine (vectorize\_datasets)
-  routines for simplified raster creation and statistics
-  integration with vector based geometry in many of the routines
-  a simplified hydrological routing library including,

   -  d-infinity flow direction
   -  support for plateau drainage
   -  weighted and unweighted flow accumulation
   -  and weighted and unweighted flow distance

Es importante aclarar que PyGeoprocessing no ha sido implementado a lo largo del presente trabajo, sin embargo, es mencionado debido a que cuenta con un grupo activo de desarrolladores y en versiones posteriores de este documento será incluido. 

Estructura de un DEM
~~~~~~~~~~~~~~~~~~~~

La estructura de una imagen satelital es compleja, pero, en forma
simplificada, consta de un conjunto de datos que caracterizan a los
datos de la imagen en sí, éstos son denominados metadatos.

Luego, los datos propiamente dichos de la imagen, donde para el caso de
los DEMs, corresponden a una única matriz cuyos valores representan la
elevación del terreno. Existen otras imágenes que cuentan con más de una
matriz, por ejemplo, una imagen Landsat, cuenta con 8 matrices,
denominadas bandas, donde cada una representa lo capturado por un sensor
determinado (por ejemplo, infrarrojo, infrarrojo medio, etc.) y además
suele tener sus propios metadatos.

Para programar y realizar cálculos sobre un DEM es necesario entender
este tipo de estructura, que en función de la biblioteca de software que
se utilice los brindará en una determinada estructura de datos. Para el
caso aquí planteado, nos basaremos en GDAL/OGR, cuyo modelo de datos
raster se observa en la siguiente figura.

.. figure:: img/gdal_design.png
    :width: 1200 px 
    :alt:
    
    Estructura de datos de un DEM


Más específicamente:

-  El dataset contiene la totalidad de los datos raster, en forma de una
   colección de bandas raster, con información que es común a todas las
   bandas.
-  Cada banda raster representa una banda, un canal o una capa de la
   imagen. Por ejemplo, una imagen RGB tendría tres bandas separadas,
   cada una para los componentes rojo, verde y azul.
-  El *raster size* especifica el ancho y alto de una imagen en píxeles
   (columnas y filas respectivamente).

-  El "georeferencing transform" da información de la conversión de
   georeferencia utilizada, es decir, a partir de las coordenadas (x, y)
   en las coordenadas geográficas, esto es, sobre la superficie
   terrestre.

-  El "coordinate system" describe las coordenadas georeferenciadas
   producidas por la transformación previa. Incluye la proyección y
   datum, como las unidades y escalas usadas por los datos raster.

-  El metadata contiene información adicional sobre el dataset completo.

Cada banda raster contiene, entre otras, lo siguiente:

-  El "raster size" de la banda. Puede coincidir con el del dataset.
-  Metadatos que proveen información extra específica de la banda.
-  Una tabla de colores que describen cómo deben mapearse los valores
   del píxel en colores.
-  Los datos raster de la banda.

Hay que destacar que GDAL provee un número de *drivers* que permiten la
lectura de varios tipos de datos geoespaciales raster. Cuando se lee un
archivo, GDAL selecciona un driver adecuado automáticamente basado en el
tipo de datos. En la escritura, sin embargo, primero se debe seleccionar
el driver deseado, luego crear el dataset que se desea escribir.

Estructura de datos geoespaciales vectoriales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si bien un DEM son datos raster, a partir de ellos se suelen calcular
mapas vectoriales o, por otro lado es una operación muy común realizar
la superposición de información geoespacial vectorial sobre los DEM.

Para el caso de datos geoespaciales vectoriales, el formato **shape**
(extensión *.shp*) se ha convertido en un estándar de facto.
Inicialmente creado por la compañía ESRI, sus especificaciones abiertas
han permitido acceder e interpretar su contenido con facilidad. OGR
provee un modelo de datos para este tipo de archivos que se muestra en
la siguiente figura.

.. figure:: img/ogr_design.png
    :width: 1200 px 
    :alt:
    
    Estructura de datos de archivos vectoriales 

Más específicamente,

-  El "data source" representa el archivo sobre el que se está
   trabajando, aunque podría ser una URL o algún otra fuente de datos.

-  El "data source" tiene uno o mas capas (layer) representando
   conjuntos de datos relacionados. Por ejemplo, un único data source
   puede representar en una capa un país, otra capa puede ser las curvas
   de nivel, etc.

-  Cada capa contiene una referencia espacial (spacial reference) y una
   lista de características (feature).

-  Las referencias espaciales especifican la proyección y el datum
   utilizado por los datos de la capa.

-  Una característica (feature) corresponde a un elemento significativo
   dentro de la capa. Por ejemplo, puede representar un estado, una
   ciudad, una ruta, isla, etc. Cada característica cuenta con una lista
   de atributos y una geometría.

-  Los atributos proveen información adicional de la característica. Por
   ejemplo, una atributo puede proveer el nombre de una ciudad de una
   característica, su población, etc.

-  Finalmente, las geometrías son estructuras de datos recursivas que
   pueden contener subgeometrías, por ejemplo, una característica país
   puede consistir en una geometría que engloba varias islas, cada una
   representada por geometrías dentro de la geometría principal "país".

Tal como GDAL, OGR provee un número de drivers que permiten leer varios
tipos de datos geoespaciales vectoriales. En la lectura, OGR selecciona
el driver correspondiente automáticamente y, durante la escritura se
debe primeramente seleccionar el driver y luego crear un nuevo data
source a escribir.

Acceso a los metadatos
~~~~~~~~~~~~~~~~~~~~~~

A continuación haremos uso de la biblioteca GDAL para acceder a los
siguientes metadatos:

-  driver,
-  dimensiones,
-  proyección,
-  coordenadas iniciales (esquina superior izquierda),
-  resolución de píxel.

Inicialmente, importamos el módulo *gdal*, vinculamos un archivo DEM y
accedemos al *dataset*.

.. code:: python

    # Importa biblioteca gdal
    import gdal
    
    # DEM filename
    fDEM = 'img_dem/etopo.tiff'
    # Acceso al dataset
    dataset = gdal.Open(fDEM, gdal.GA_ReadOnly)

La variable *dataset* es una instancia al archivo del DEM, a partir de
la cual se puede acceder a los metadatos:

.. code:: python

    # Driver
    print '- Driver = ', dataset.GetDriver().LongName
    
    # Dimensiones: filas, columnas, bandas
    print '- Filas = ', dataset.RasterYSize
    print '- Columnas = ', dataset.RasterXSize
    print '- Bandas = ', dataset.RasterCount
    
    # Coordenadas y tamaño de pixel
    geotransform = dataset.GetGeoTransform()
    print '- Origen = ', geotransform[0], geotransform[3]
    print '- Tamaño de pixel = ', geotransform[1], geotransform[5]
    
    # Proyección
    print '- Proyección = ',dataset.GetProjection()


.. parsed-literal::

    - Driver =  GeoTIFF
    - Filas =  480
    - Columnas =  320
    - Bandas =  1
    - Origen =  -68.402293319 -26.9133803204
    - Tamaño de pixel =  0.016666666667 -0.016666666667
    - Proyección =  GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,
                    298.2572235604902,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG",
                    "6326"]],PRIMEM["Greenwich",0],UNIT["degree",
                    0.0174532925199433],AUTHORITY["EPSG","4326"]]


Acceso a los datos raster
~~~~~~~~~~~~~~~~~~~~~~~~~

Los datos raster son representados como una matriz donde cada celda se
corresponde con una parcela del terreno cuyo tamaño dependerá de la
resolución espacial del sensor que la obtuvo.

Tal como se explicó previamente, una imagen satelital puede contener
varias matrices de datos raster, también denominadas bandas, para el
caso de los DEM, generalmente existe una única banda. Para conocer la
cantidad de bandas presentes en un dataset invocamos el método
*RasterCount*.

.. code:: python

    nband = dataset.RasterCount
    print '- Cantidad de bandas =', nband


.. parsed-literal::

    - Cantidad de bandas = 1


Con la ejecución previa observamos la presencia de una única banda,
inicialmente obtendremos una instancia que nos brindará información
general sobre los datos raster almacenados como el tipo de datos,
algunos estadísticos globales, etc.

.. code:: python

    # Instancia a la banda
    band = dataset.GetRasterBand(nband)
    
    # Tipo de dato numérico
    print '- Tipo numérico de datos =',gdal.GetDataTypeName(band.DataType)
    
    # Mínimo y máximo
    min = band.GetMinimum()
    max = band.GetMaximum()
    print '- Min = %.3f, Max = %.3f' % (min,max)
    
    # Mapa de colores incluído?
    print "- Mapa de colores: ", band.GetRasterColorTable() 
    
    # otros estadísticos
    (min, max, mean, stddev) = band.GetStatistics(False, True)
    print "- Media y stddev: ", mean, stddev


.. parsed-literal::

    - Tipo numérico de datos = Int16
    - Min = 69.000, Max = 6154.116
    - Mapa de colores:  None
    - Media y stddev:  731.075 794.3152


Lectura de datos raster
~~~~~~~~~~~~~~~~~~~~~~~

Método ReadAsArray()
^^^^^^^^^^^^^^^^^^^^

Una vez que tenemos la instancia a la banda de datos raster, solo queda
acceder a ellos como una matriz. Para eso utilizaremos el método
automático provisto por GDAL denominado *ReadAsArray()*.

Este método es el más directo para realizar una lectura completa de los
datos raster a una matriz. Más específicamente, la estructura de datos
que nos retorna es un **arreglo n-dimensional** (numpy.ndarray) incluído
en la biblioteca **Numpy**, por lo que será útil importarla. Además,
durante la lectura se realiza la conversión automática a datos
flotantes.

`NumPy <http://www.numpy.org/>`__ es una biblioteca de cálculo
científico para Python que provee estructuras de datos eficientes para
el cálculo. Si bien no se hará énfasis en su uso, es importante destacar
que provee cientos de funcionalidades de procesamiento de gran utilidad.

.. code:: python

    # importa numpy
    import numpy as np
    
    # lee raster y convierte datos a flotante
    dem = band.ReadAsArray().astype(np.float)

En la variable dem tenemos el acceso a la matriz que contiene los
valores de elevación de un terreno. A continuación se corrobora el tipo
de datos y se muestra la cantidad de filas y columnas de la matriz, que
coincidirá con el obtenido previamente.

.. code:: python

    print "Tipos de datos: ", type(dem)
    print "Dimensión: ", dem.shape


.. parsed-literal::

    Tipos de datos:  <type 'numpy.ndarray'>
    Dimensión:  (480, 320)


Método ReadRaster()
^^^^^^^^^^^^^^^^^^^

El método previo se destaca por su simplicidad y eficacia, y será el
utilizado en el resto de la guía, sin embargo es importante destacar que
no es el más eficiente. Otro método consiste en leer por bloques los
datos en un buffer convirtiéndolos al formato correspondiente. La
siguiente línea realiza esto:

.. code:: python

    sline = band.ReadRaster(0, 0, band.XSize, 1, band.XSize, 1, gdal.GDT_Float32)

La variable *sline* es de tipo string (cadena de caracteres), y
contiene la cantidad de bytes equivalente a 4\*(Nro de columnas), donde
el número de columnas es la dimensión x (cantidad de columnas,
band.XSize). Esto debe ser convertido a valores numéricos de Python
usando el módulo *struct* de la biblioteca estándar e ir agregando cada
fila a la matriz final.

A continuación se observa un ejemplo de cómo sería el proceso completo
de lectura de una imagen:

.. code:: python

    import struct
    tuple_of_floats = struct.unpack('f' * band.XSize, scanline)

    datarray = np.zeros( ( band.YSize,band.XSize ), gdt2npy[band.DataType] )
    # create loop based on YAxis (i.e. num rows)
    for i in range(band.YSize):
        # read lines of band    
        sline = band.ReadRaster( 0, i, band.XSize, 1, band.XSize, 1, band.DataType)
        # unpack from binary representation
        tuple_of_vals = struct.unpack(gdt2struct[band.DataType] * band.XSize, sline)
        # add tuple to image array line by line 
        datarray[i,:] = tuple_of_vals

Cabe destacar que existen bibliotecas para realizar esta tarea de forma
mas transparente como por ejemplo
`rasterio <https://github.com/mapbox/rasterio>`__ o
`PyRaster <https://github.com/talltom/PyRaster/blob/master/rasterIO.py>`__,
sin embargo, a los fines de mantener la claridad del presente trabajo se
hará uso del método *ReadAsArray()* mencionado previamente.

Cerrar el dataset
~~~~~~~~~~~~~~~~~

Si no vamos a trabajar mas con el dataset, eliminamos la instancia
asignándole None.

.. code:: python

    dataset = None

Visualización
~~~~~~~~~~~~~

Una vez que se cuenta con una matriz de valores numéricos solamente es
necesario una biblioteca de graficación que permita visualizarla.

La biblioteca Python más utilizada para visualización científica
-principalmente en 2 dimensiones- es
`Matplotlib <http://matplotlib.org>`__, además es completamente
compatible con las estructuras de datos de NumPy, por lo que
compatibiliza perfectamente con GDAL.

.. code:: python

    %matplotlib inline
    # alternativa  %matplotlib notebook
    from matplotlib import pyplot as plt
    from matplotlib import cm # mapa de colores
    
    plt.title('DEM')
    plt.grid()
    plt.xlabel('nro de columnas')
    plt.ylabel('nro de filas')
    # el ; evita la salida
    plt.imshow(dem,cmap = cm.gist_earth);




.. figure:: img/output_19_0.png
    :width: 800 px

    Visualización 2D con Matplotlib


Para visualización tridimensional se utiliza
`Mayavi <http://docs.enthought.com/mayavi/mayavi/>`__, la biblioteca
preferida para visualización científica en 3D. Mayavi también es
completamente compatible con estructuras de datos NumPy, por lo que su
integración es directa. A continuación se observa un fragmento de código
inofensivo para una visualización 3D de la matriz almacenada en la
variable dem:

.. code:: python

    from mayavi import mlab
    #%gui wx
    mlab.surf(dem, colormap='gist_earth',warp_scale=0.01)

.. figure:: img/dem_mayavi01.png
    :width: 1100 px

    Visualización 3D con Mayavi

Procesamiento
-------------

Para el procesamiento sobre los DEMs solo basta con realizar los
cálculos deseados sobre la matriz de datos. Implementar los algoritmos
para el cálculo de mapas pendientes, aspecto, etc, caen fuera del
alcance de esta guía introductoria, al menos en la versión inicial.

De modo que para realizar este tipo de procesamiento se menciona a
continuación el modo de realizarlo utilizando las herramientas que
provee GDAL, PyDEM y PyGeoprocessing.

Junto con la instalación de la biblioteca GDAL se distribuyen un
conjunto de programas utilitarios para el procesamiento de DEMs . Uno de
ellos es *gdaldem*, que permite calcular:

-  averge aspect,
-  hillshading,
-  slope,
-  roughness,
-  terrain ruggedness index (TRI) y,
-  topographic position index (TPI).

Todo estos mapas se generan como imágenes raster tif (formato GeoTiff).

Procesamiento con gdaldem
~~~~~~~~~~~~~~~~~~~~~~~~~

Si bien se hará uso principalmente de gdaldem, a continuación se listan
las utilidades incluidas en GDAL que pueden ser fácilmente utilizadas en
el desarrollo de software.

+-----------------------+--------------------------------------------------------------------------------------------------------------+
| Utilidad              | Descripción                                                                                                  |
+=======================+==============================================================================================================+
| gdalinfo              | report information about a file.                                                                             |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_translate       | Copy a raster file, with control of output format.                                                           |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdaladdo              | Add overviews to a file.                                                                                     |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdalwarp              | Warp an image into a new coordinate system.                                                                  |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdaltindex            | Build a MapServer raster tileindex.                                                                          |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdalbuildvrt          | Build a VRT from a list of datasets.                                                                         |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_contour         | Contours from DEM.                                                                                           |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdaldem               | Tools to analyze and visualize DEMs.                                                                         |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| rgb2pct.py            | Convert a 24bit RGB image to 8bit paletted.                                                                  |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| pct2rgb.py            | Convert an 8bit paletted image to 24bit RGB.                                                                 |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_merge.py        | Build a quick mosaic from a set of images.                                                                   |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal2tiles.py         | Create a TMS tile structure, KML and simple web viewer.                                                      |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_rasterize       | Rasterize vectors into raster file.                                                                          |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdaltransform         | Transform coordinates.                                                                                       |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| nearblack             | Convert nearly black/white borders to exact value.                                                           |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_retile.py       | Retiles a set of tiles and/or build tiled pyramid levels.                                                    |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_grid            | Create raster from the scattered data.                                                                       |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_proximity.py    | Compute a raster proximity map.                                                                              |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_polygonize.py   | Generate polygons from raster.                                                                               |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_sieve.py        | Raster Sieve filter.                                                                                         |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_fillnodata.py   | Interpolate in nodata regions.                                                                               |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdallocationinfo      | Query raster at a location.                                                                                  |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdalsrsinfo           | Report a given SRS in different formats. (GDAL >= 1.9.0)                                                     |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdalmove.py           | Transform the coordinate system of a file (GDAL >= 1.10)                                                     |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal\_edit.py         | Edit in place various information of an existing GDAL dataset (projection, geotransform, nodata, metadata)   |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdal-config           | Get options required to build software using GDAL.                                                           |
+-----------------------+--------------------------------------------------------------------------------------------------------------+
| gdalmanage            | Identify, copy, rename and delete raster.                                                                    |
+-----------------------+--------------------------------------------------------------------------------------------------------------+

A continuación se describe el modo de uso y los parámetros que acepta
gdaldem, ya que posteriormente será incluido en un software desarrollado
para este fin, denominado DEMystiPy.

Average aspect
^^^^^^^^^^^^^^

Es un mapa raster que informa hacia la dirección que mira la cara de una
pendiente. La herramienta **gdaldem** permite generar este mapa
utilizando la opción *aspect* como parámetro, además del DEM de entrada
y el nombre de archivo de salida. Por ejemplo,

.. code:: bash

    $gdaldem aspect etopo.bil etopo_aspect.tif

Por defecto realiza el cálculo en grados, cuya interpretación es: - 0:
norte - 90: este - 180: sur - 270: oeste

El modo de ejecución desde la terminal es:

.. code:: bash

    gdaldem aspect etopo.bil etopo_aspect2.bil

Para invocarlo desde un programa Python se hace uso del módulo incluido
en la biblioteca estándar de Python denominado *subprocess*.

.. code:: python

    import subprocess as sup
    # Archivo de entrada
    iDEM = "img_dem/etopo.bil"
    # Archivo de salida
    oFILE = "etopo_aspect.tif"
    sms = sup.check_output(["gdaldem", "aspect", iDEM, oFILE, "-zero_for_flat"])
    print sms


.. parsed-literal::

    0...10...20...30...40...50...60...70...80...90...100 - done.
    


Del manual que provee gdaldem es posible ver las opciones que acepta:

.. parsed-literal::

    To generate an aspect map from any GDAL-supported elevation raster
             Outputs a 32-bit float raster with pixel values from 0-360 
             indicating azimuth:
               gdaldem aspect input_dem output_aspect_map
                           [-trigonometric] [-zero_for_flat]

Hillshading
^^^^^^^^^^^

Crea un raster con el efecto de sombra a partir del DEM. Simula una
fuente de luz cuya dirección es, por defecto, en la esquina superior
izquierda con una inclinación de 45°.

Las opciones para invocarlos se muestran a continuación:

.. parsed-literal::

    - To generate a shaded relief map from any GDAL-supported elevation raster :
               gdaldem hillshade input_dem output_hillshade
                       [-z ZFactor (default=1)] [-s scale* (default=1)]
                       [-az Azimuth (default=315)] [-alt Altitude (default=45)]
                       [-combined]

La opción *-combined* puede ser utilizada para generar un resultado con
mayor brillo. En caso que el DEM sea de una región con pendientes muy
bajas, se puede utilizar la opción *-z factor* para mostrarlas con mayor
contraste, donde factor multiplica los valores del DEM para calcular el
efecto de sombreado.

.. parsed-literal:: 

     hillshade
         This command outputs an 8-bit raster with a nice shaded relief effect.
         It’s very useful for visualizing the terrain. You can optionally specify
         the azimuth and altitude of the light source, a vertical exaggeration 
         factor and a scaling factor to account for differences between vertical
         and horizontal units.

      The value 0 is used as the output nodata value.

      The following specific options are available :

       -z zFactor:
           vertical exaggeration used to pre-multiply the elevations

       -s scale:
           ratio of vertical units to horizontal. If the horizontal unit of
           the source DEM is degrees (e.g Lat/Long WGS84 projection), you can 
           use scale=111120 if the vertical units are meters (or scale=370400 
           if they are in feet)

       -az azimuth:
           azimuth of the light, in degrees. 0 if it comes from the top of the
           raster, 90 from the east, ... The default value, 315, should rarely
           be changed as it is the value generally used to generate shaded maps.

       -alt altitude:
           altitude of the light, in degrees. 90 if the light comes from above 
           the DEM, 0 if it is raking light.

       -combined combined shading:
           (starting with GDAL 1.10) a combination of slope and oblique shading.

En el fragmento de programa siguiente se invoca modificando la altitud
de la luz (-alt 90).

.. code:: python

    import subprocess as sup
    iDEM = "img_dem/etopo.bil"
    oFILE = "etopo_hillshading.tif"
    sms = sup.check_output(["gdaldem", "hillshade", iDEM, oFILE, "-alt", "90"])
    print sms


.. parsed-literal::

    0...10...20...30...40...50...60...70...80...90...100 - done.
    


Slope
^^^^^

A partir del DEM de entrada se genera un mapa raster de 32bits de
valores flotantes correspondientes a la pendiente. Permite elegir entre
grados o pendiente y, en caso que las unidades horizontales difieran de
las de altura se puede suministrar un factor de escala. El valor -9999
es usado como salida para celdas sin datos.

Las especificaciones provistas por el manual:

.. parsed-literal::

   -p :
       if specified, the slope will be expressed as percent slope. 
       Otherwise, it is expressed as degrees

   -s scale:
       ratio of vertical units to horizontal. If the horizontal unit 
       of the source DEM is degrees (e.g Lat/Long WGS84 projection), 
       you can use scale=111120 if the vertical units are meters (or
       scale=370400 if they are in feet)
       
   gdaldem slope input_dem output_slope_map
     [-p use percent slope (default=degrees)] [-s scale* (default=1)]

Terrain Ruggedness Index (TRI)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

El TRI es definido como la diferencia promedio entre el píxel central y
las celdas que lo rodean (Wilson et al 2007, Marine Geodesy 30:3-35). La
salida es un raster de banda única y el valor -9999 corresponde para los
valores sin datos.

Las especificaciones provistas por el manual:

.. parsed-literal::

     - To generate a Terrain Ruggedness Index (TRI) map from any 
        GDAL-supported elevation raster:
     gdaldem TRI input_dem output_TRI_map

Topographic Position Index (TPI)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

El TPI es definido como la diferencia entre el píxel central y la media
de las celdas vecinas (Wilson et al 2007, Marine Geodesy 30:3-35). El
valor -9999 corresponde para los valores sin datos.

Las especificaciones provistas por el manual:

.. code:: bash

      - To generate a Topographic Position Index (TPI) map from any
        GDAL-supported elevation raster:
     gdaldem TPI input_dem output_TPI_map

Rugosidad
^^^^^^^^^

La rugosidad se define como la mayor diferencia entre el píxel central y
sus celdas vecinas (2007, Marine Geodesy 30:3-35). Al igual que en los
dos casos previos, el valor -9999 corresponde para los valores sin
datos.

Las especificaciones provistas por el manual:

.. parsed-literal::

    - To generate a roughness map from any GDAL-supported elevation raster:
       gdaldem roughness input_dem output_roughness_map
                   [-compute_edges] [-b Band (default=1)] [-of format] [-q]

Curvas de nivel
^^^^^^^^^^^^^^^

Para el cálculo de las curvas de nivel se utiliza la herramienta
`gdal\_contour <http://gdal.org/1.11/gdal_contour.html>`__. Este
programa genera un archivo vectorial a partir del DEM raster.

Las especificaciones provistas por el manual:

.. code:: bash

    Usage: gdal_contour 
        [-b <band>] [-a <attribute_name>] [-3d] [-inodata]
        [-snodata n] [-i <interval>]
        [-f <formatname>] [[-dsco NAME=VALUE] ...] [[-lco NAME=VALUE] ...]
        [-off <offset>] [-fl <level> <level>...]
        [-nln <outlayername>]
        <src_filename> <dst_filename>   

Para el DEM procesado previamente, el comando es:

::

    gdal_contour -a elev -nln curvas etopo.bil etopo_contour.shp -i 10.0

Para corroborar la generación de las curvas de nivel visualizamos el
archivo shape en SAGA.

.. figure:: img/shape_screenshot.png
    :width: 1200 px

    Líneas de contorno visualizadas en SAGA 

También es posible analizarlo desde Python utilizando el módulo OGR. En
el siguiente ejemplo, se itera sobre las características y atributos las
líneas de contorno y se las almacena en un archivo de texto.

.. code:: python

    # import modules
    import ogr

    # open the output text file for writing
    file = open('etopo_shp_features_atributes.txt', 'w')
    
    # get the shapefile driver
    driver = ogr.GetDriverByName('ESRI Shapefile')
    
    # open the data source
    datasource = driver.Open('img_dem/etopo_contour.shp', 0)
    
    # get the data layer
    layer = datasource.GetLayer()
    
    # loop through the features in the layer
    feature = layer.GetNextFeature()
    while feature:
        # get the attributes
        id = feature.GetFieldAsString('id')
        cover = feature.GetFieldAsString('cover')
    
        # get the x,y coordinates for the point
        geom = feature.GetGeometryRef()
        x = str(geom.GetX())
        y = str(geom.GetY())
    
        # write info out to the text file
        file.write(id + ' ' + x + ' ' + y + ' ' + str(cover) + '\n')
    
        # destroy the feature and get a new one
        feature.Destroy()
        feature = layer.GetNextFeature()
    # close the data source and text file
    datasource.Destroy()
    file.close()

Parte del archivo de salida se observa a continuación:

::

    0 -66.193959384 -26.9133803204 None
    1 -66.193959384 -26.9217136537 None
    2 -67.5106258314 -26.9217136537 None
    3 -64.1772104002 -26.9133803204 None
    4 -64.7198859116 -26.9133803204 None
    5 -65.1604616358 -26.9133803204 None
    ...

Ya que no hay ningún tipo de cobertura, ni atributo de una capa,
solamente se observan las coordenadas de los puntos que forman las
curvas de nivel.

Procesamiento con PyDEM
~~~~~~~~~~~~~~~~~~~~~~~

El procesamiento con PyDEM es muy simple y directo. El tipo de archivo
de entrada que soporta es GeoTiff, por lo que primeramente se debe
convertir a este formato utilizando la herramienta gdal\_translate como
se muestra a continuación:

::

    gdal_translate img_dem/aster30.bil img_dem/aster30.tiff

Una vez realizada la conversión a tiff, importamos el módulo y asignamos
el nombre de archivo del DEM a una variable (debe estar en coordenadas
WGS84).

.. code:: python

    from pydem.dem_processing import DEMProcessor
    fDEM = 'img_dem/etopo.tiff'

Luego, instaciamos la clase DEMProcessor:

.. code:: python

    dem_proc = DEMProcessor(fDEM)

Calculamos la magnitud de la pendiente (mag) y el aspecto (aspect):

.. code:: python

    mag, aspect = dem_proc.calc_slopes_directions()

Vemos que ambos son matrices numpy de 480x320 cuyos datos son flotantes
de 64 bits.

.. code:: python

    print type(mag)
    print type(mag[0][0])
    print mag.shape
    
    
    print type(aspect)
    print type(aspect[0][0])
    print aspect.shape


.. parsed-literal::

    <type 'numpy.ndarray'>
    <type 'numpy.float64'>
    (480, 320)
    <type 'numpy.ndarray'>
    <type 'numpy.float64'>
    (480, 320)


Con estas dos matrices es posible realizar la visualización de ambos
cálculos.

.. code:: python

    %matplotlib inline
    # alternativa  %matplotlib notebook
    from matplotlib import pyplot as plt
    
    plt.title('Slope Magnitude')
    plt.imshow(mag);



.. image:: img/output_37_0.png
    :width: 600px


.. code:: python

    plt.title('Aspect')
    plt.imshow(aspect);



.. image:: img/output_38_0.png
    :align: center
    :width: 600 px

Conclusiones y comentarios finales
----------------------------------

El desarrollo realizado a lo largo de la guía ha brindado un
conocimiento lo suficientemente acabado como para tener una base mínima
necesaria para comenzar a desarrollar software de procesamiento y
visualización de Modelos Digitales de Elevación.

Se utilizaron módulos y herramientas externas relativamente actuales que
permiten ser incorporadas sin mayor esfuerzo a un software propio, lo
que no solo tiene la utilidad del cálculo resuelto, sino que posibilita
comprar con desarrollos propios.

Como resultado de este proceso se ha desarrollado un software -por el
momento- mínimo que incluye lo mencionado en el presente trabajo y que
se describe someramente a continuación.

Desarrollo de software DEMystiPy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El software que se presenta involucra el uso de las herramientas
previamente descriptas. DEMystiPy está completamente desarrollado en
Python, es software libre, publicado bajo la licencia GPL v3 y, puede
ser descargado y modificado desde el repositorio
público https://gitlab.com/emilopez/DEMystiPy

Algunas de los procesamientos mencionados en el presente trabajo aún no
han sido incorporados, a continuación se mencionan las características
de DEMystiPy:

-  Interfaz de usuario gráfica (GUI)
-  Visualización de DEMs y mapas raster calculados

   -  zoom in/out
   -  exportar como imagen png, jpg, etc.

-  Elección del mapa de colores para visualización
-  Referencia del valor numérico del color
-  Muestra información global del DEM:

   -  Driver
   -  Proyección
   -  Tamaño de píxel
   -  XSize, YSize
   -  Coordenadas de la imagen (superior izquierda, inferior izquierda,
      superior derecha, inferior derecha, centro)

-  Cálculos realizados utilizando la herramienta gdaldem:

   -  Average Aspect
   -  Hillshading
   -  Slope
   -  Roughness
   -  Terrain Ruggedness Index (TRI)
   -  Topographic Position Index (TPI)


Es importante destacar que aún no es la definitiva por lo que puede
contener errores y fallas inesperadas. A continuación se muestran una
serie de capturas de pantalla de la aplicación funcionando.


.. figure:: img/demy01.png
    :width: 1700 px

    DEMystiPy visualizando DEM aster30.bil

.. figure:: img/demy04.png
    :width: 1700 px

    Zoom sobre aster30.bil y cálculo de mapas

.. figure:: img/demy05.png
    :width: 1700 px

    Hillshanding de aster30.bil

.. figure:: img/demy06.png
    :width: 1700 px

    Hillshanding de aster30.bil con mapa de color gist_earth

.. figure:: img/demy07.png
    :width: 1700 px

    Average aspect de aster30.bil con mapa de color flag


Referencias y enlaces útiles
----------------------------

-  Ueckermann, M. (2015). Global Hydrology Analysis Using Python. SciPy
   2015. Austin, Texas. 6 al 12 Julio.
-  Westra, E. (2013). Python Geospatial Development. Second Edition.
   Packt Publishing. ISBN 978-1-78216-152-3.
-  Lawhead, J. (2013). Learning Geospatial Analysis with Python. Packt
   Publishing. ISBN 978-1-78328-113-8.
-  `Taudem <https://github.com/dtarb/TauDEM>`__
-  `Acelerar cálculos sobre un
   raster <http://geoinformaticstutorial.blogspot.no/2014/01/raster-calculations-with-numpywhere.html>`__
-  `Convertir shape a
   raster <http://geoinformaticstutorial.blogspot.no/2012/11/convert-shapefile-to-raster-with-gdal.html>`__
-  `Reading Raster Data with
   GDAL <http://www.gis.usu.edu/~chrisg/python/2009/lectures/ospy_slides4.pdf>`__
-  `Matplotlib
   colormaps <http://wiki.scipy.org/Cookbook/Matplotlib/Show_colormaps>`__
-  `ArcGIS
   Slope <http://resources.arcgis.com/en/help/main/10.1/index.html#/How_Slope_works/009z000000vz000000/>`__
-  `ArcGIS flow direction
   1 <http://resources.arcgis.com/en/help/main/10.1/index.html#/How_Flow_Direction_works/009z00000063000000/>`__
-  `ArcGIS flow direction
   2 <http://resources.arcgis.com/en/help/main/10.1/index.html#//009z00000052000000>`__
-  `ArcGIS flow
   acumulation <http://resources.arcgis.com/en/help/main/10.1/index.html#/How_Flow_Accumulation_works/009z00000062000000/>`__
-  `ArcGIS
   curvatura <http://resources.arcgis.com/en/help/main/10.1/index.html#/How_Curvature_works/009z000000vs000000/>`__
-  `Python GIS
   resources <http://www.gislounge.com/python-and-gis-resources/>`__
-  `GDAL DEM
   utilities <http://blog.perrygeo.net/2006/02/08/gdal-based-dem-utilities/>`__
-  `GDAL
   slope <https://apollomapping.com/blog/gdal-calculating-slope>`__
-  `Slope
   1 <https://spectraldifferences.wordpress.com/2014/08/21/create-a-slope-layer-from-a-dem/>`__
-  `Slope 2 <https://github.com/MiXIL/calcSlopeDegrees>`__
-  `GDAL cheat sheet <https://github.com/dwtkns/gdal-cheat-sheet>`__
-  `The Remote Sensing and GIS Software Library
   (RSGISLib) <http://www.sciencedirect.com/science/article/pii/S0098300413002288>`__
-  `Raster I/O
   Simplification <https://bitbucket.org/chchrsc/rios/src>`__
-  `GIS, Data Science and Software
   Solutions <http://www.perrygeo.com/index2.html>`__
-  `Estadísticas sobre regiones
   (shp) <https://github.com/perrygeo/python-raster-stats>`__
-  `Python shape lib
   1 <https://pythonhosted.org/Python%20Shapefile%20Library/>`__
-  `Python shape lib 2 <https://github.com/GeospatialPython/pyshp>`__


.. |date| date::
.. |time| date:: %H:%M

Este documento fue creado el |date| a las |time|
